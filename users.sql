-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 14 2016 г., 18:36
-- Версия сервера: 5.5.50
-- Версия PHP: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `users`
--

-- --------------------------------------------------------

--
-- Структура таблицы `alerts`
--

CREATE TABLE IF NOT EXISTS `alerts` (
  `id` int(11) NOT NULL,
  `sector` int(11) NOT NULL,
  `threat` text NOT NULL,
  `tenno` int(11) DEFAULT NULL,
  `lvl` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `alerts`
--

INSERT INTO `alerts` (`id`, `sector`, `threat`, `tenno`, `lvl`) VALUES
(1, 1, 'Diversion on grineer dockstation', NULL, 2),
(2, 4, 'Corpus detected', NULL, 1),
(3, 7, 'Grineer warmaster detected', NULL, 3),
(4, 2, 'Infestation must be secured', NULL, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `sectors`
--

CREATE TABLE IF NOT EXISTS `sectors` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `planet` text NOT NULL,
  `img` text
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sectors`
--

INSERT INTO `sectors` (`id`, `name`, `planet`, `img`) VALUES
(1, 'Draco', 'Ceres', 'src/draco.jpg'),
(2, 'Seimeni', 'Ceres', 'src/seimeni.jpg'),
(3, 'Coba', 'Earth', 'src/coba.jpg'),
(4, 'Tikal', 'Earth', 'src/tikal.jpg'),
(5, 'Akkad', 'Eris', 'src/akkad.jpg'),
(6, 'Zabala', 'Eris', 'src/zabala.jpg'),
(7, 'Cholistan', 'Europe', 'src/cholistan.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `login` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `surname` text NOT NULL,
  `name` text NOT NULL,
  `callname` text NOT NULL,
  `ship` text NOT NULL,
  `img` text
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `surname`, `name`, `callname`, `ship`, `img`) VALUES
(1, 'hello', '1234', '', '', '', '', NULL),
(4, 'exalt', '4321', '', '', '', '', NULL),
(5, 'kernous', '2345', '', '', '', '', NULL),
(6, 'user12', '98765', '', '', '', '', NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `alerts`
--
ALTER TABLE `alerts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sectors`
--
ALTER TABLE `sectors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `alerts`
--
ALTER TABLE `alerts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `sectors`
--
ALTER TABLE `sectors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
